// import directives
const express = require("express");
const mongoose = require("mongoose");
const app = express();	// initiate server
const PORT = 4000; 	// declare porrt address

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// connect to MongoDB
mongoose.connect('mongodb+srv://kaisertabuada:2qjhsn9Q@batch139.f3dzn.mongodb.net/users?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});

// notify if connect or not
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));

// schemas
const userSchema = new mongoose.Schema(
	{
		username: String,
		password: String,
		status: {
			type: String,
			default: "registered"
		}
	}
);

// models
const User = mongoose.model("User", userSchema);

// routes
// schema test
app.get("/signup", (req, res) => {
	User.find({} ,(err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.send(result);
		}
	})
});
// main activity objective
app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		//console.log(result);
		if(result != null && result.username == req.body.username) {
			return res.send(`User already exist!`);
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})
			newUser.save((err, savedTask) => {
				if(err) {
					return console.error(err)
				} else {
					return res.send(`User ${req.body.username} succesfully register!`)
				}
			});
		}
	});
});

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));